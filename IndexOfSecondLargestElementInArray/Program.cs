﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3, 4, 5, 6, 7 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        int stt = 0;
        int max1 = x[0];
        int dem = 0;

        if (x.Length == 1)
        {
            return -1;
        }
        else
        {
            for (int i = 0; i < x.Length; i++)
            {
                if (x[i] > max1)
                    max1 = x[i];

            }
            int max2 = 0;
            for (int i = 0; i < x.Length; i++)
            {
                if (x[i] == max1)
                {
                    dem = i;
                }

                if (x[i] > max2 && x[i] != max1)
                {
                    max2 = x[i];
                    stt = i;
                }
            }
            return stt;
        }

        // kết quả hàm sau khi thực thi là vị trí(index) của số lớn thứ 2 trong mảng
        throw new NotImplementedException();
    }
}
